/* $Id$ */

/*
 *
 * Copyright (C) 2004 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "util/avutil.h"
#include <sysexits.h>

extern int truncfd;
extern off_t truncpos;
extern char *deleteme;
extern char *progname;

/* local.c */
extern char opt_separator;
extern const char *opt_touser;
extern const char *opt_extra;
extern const char *opt_smuser;
extern const char *opt_fallback;
extern const char *opt_bouncefrom;
extern const char *opt_sendmail;
extern const char *opt_default;
void mysetenv (const char *var, const char *val, int len);

/* copymsg.c */
extern const char *opt_recip;
extern const char *opt_tmplate;
extern const char *opt_from;
extern char *msg_ufline;
extern char *msg_rpline;
extern char *msg_dtline;
void initfile (char **path, int *wfd, int *rfd, struct stat *sbp);
void set_msgvars (struct lnbuf *bufp, FILE *in);
int nocopymsg (FILE *in);
void copymsg (int outfd, FILE *in);

/* child.c */
struct passwd;
struct passwd *validuser (const char *user);
int become_user (struct passwd *pw, int grouplist, int cd);
int child (struct passwd *pw, int parent_fd, int message_fd);

/* lock.c */
int opt_noflock;
int opt_fcntl;
int opt_nowait;
int tmperr (int err);
int stat_unchanged (const struct stat *sb1, const struct stat *sb2);
int dotlock (int *fdp, char **lockfilep, const char *path, int *lfdp);

/* mailbox.c */
int deliver_mbox (const char *path, int mfd, int use_readp);
int deliver_maildir (const char *path, int mfd, int use_readp);
