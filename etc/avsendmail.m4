dnl $Id$
dnl
dnl Avenger configuration for sendmail
dnl
dnl Include this file in your sendmail ".mc" file to get
dnl support for the avenger.local mailer.
dnl
ifelse(SEPARATOR, `SEPARATOR', `define(`SEPARATOR', +)')dnl
ifelse(SEPARATOR, +,, `define(`confOPERATORS', confOPERATORS`'SEPARATOR)')dnl

PUSHDIVERT(7)
Mavenger,	P=/usr/local/libexec/avenger.local,
		F=lsDFMAqSfhn9,
		S=EnvFromL/HdrFromL, R=EnvToL/HdrToL,
		T=DNS/RFC822/X-Unix,
		A=avenger.local --separator SEPARATOR -D $h -d $u
POPDIVERT

LOCAL_CONFIG

C+SEPARATOR

Kavdomain text -z: -k0 -v1 -o /etc/avenger/domains
Kavalias text -z: -k0 -v1 -o /etc/avenger/aliases
Kavsysaliases implicit -m ALIAS_FILE
Kavsyspasswd user -m

LOCAL_RULE_0

dnl
dnl Perform lookup of domain name in /etc/avenger/domains.  If
dnl hostname is not there, then return; don't resolve to avenger
dnl mailer.
dnl
dnl However, because masquerading happens in sendmail.cf rather than
dnl in submit.cf, we can sometimes end up with the hostname ($j)
dnl instead of the masquerading host name ($M)--especially for locally
dnl generated bounce messages.  So if $j is not in
dnl /etc/avenger/domains, we try $M as well.
dnl
R$+ < @ $+ . >		$: < $(avdomain $2 $: @ $) > $1 < @ $2 . >
ifdef(`_MASQUERADE_ENVELOPE_',
`R<@> $~E < @ $j . >	$: < $(avdomain $M $: @ $) > $1 < @ $j . >')
R<@> $+ < @ $+ . >	$@ $1 < @ $2 . >
R<$+ $=+> $+ < @ $+ . >	$: $3 < @ $4 . > $| $1 $2 $3
R<$+> $+ < @ $+ . >	$: $2 < @ $3 . > $| $1
R<> $+ < @ $+ . >	$: $1 < @ $2 . > $| $1

dnl
dnl Do substitution based on /etc/avenger/aliases file
dnl When we get a username back, of the form "user" or "user+extra",
dnl we make sure that user exists in the password file.  This is
dnl because we can't use the "w" flag for the mailer, because the
dnl user in the avenger mailer is "user+extra", not just "user".
dnl
R$+ < @ $+ . > $| $+	$: $1 < @ $2 . > $| $>avuser $3 <>
R$+ < @ $+ . > $| $+ <>	$@ $3
R$+ < @ $+ . > $| $-	$: $1 < @ $2 . > $| $(avsyspasswd $3 $: < @ > $)
R$+ < @ $+ . > $| $- $=+ $+
			$: $1 < @ $2 . > $| $(avsyspasswd $3 $: < @ > $) $4 $5
dnl R$+ < @ $=w . > $| <@> $*
dnl 			$@ $1 < @ $2 . >
R$+ < @ $+ . > $| <@> $*
			$#error $@ 5.1.1 $: "User unknown"
R$+ < @ $+ . > $| $+	$#avenger $@ $1 @ $2 $: $>avuser $3 <>

dnl
dnl Recursively substitute users from /etc/avenger/aliases file.
dnl On input expects "user <>" or "user+extra <>".  The <> is just
dnl to help with the recursion.
dnl
dnl Note that if we ever hit an alias in the system aliases file, we
dnl stop and return immediately.  This is so that you can have a
dnl mailing list in /etc/mail/aliases, which gets expanded to one set
dnl of mail destinations, while for filtering purposes asmtpd
dnl redirects processing to a particular user's rcpt file.
dnl
Savuser

R$+ <>			$: < $(avsysaliases $1 $: @ $) > $1 <>
R<@> $+ <>		$: $1 <>
R<$+> $+ <>		$@ $2 <>
R$+ <$*>		$: < $(avalias $1 $: @ $) > $1 < $2 >
R<@> $* $~+ <$*>	<@> $1 < $2 $3 >
R<@> $+ $=+ <$*>	$@ $>avuser $1 < $2 $3 >
R<@> <$*>		$@ $1
R<$+> $+ <$*>		$@ $>avuser $1 $3 <>
