// -*-c++-*-
/* $Id$ */

/*
 *
 * Copyright (C) 2005 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#ifndef _ASYNC_SSL_H_
#define _ASYNC_SSL_H_ 1

#include "async.h"
#include "aios.h"
#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/rand.h>

inline str
ssl_err (int en = 0)
{
  int e = ERR_get_error ();
  const char *msg = ERR_reason_error_string (e);
  str msgs (msg ? msg : (en > 0 ? strerror (en) : "SSL library error"));
  ERR_clear_error ();
  return msgs;
}

class aiossl : public aios {
  SSL *ssl;
  BIO *bss;
  BIO *inbss;
  ptr<suio> oldout;
  cbv::ptr vcb;

  bool server;
  bool need_handshake;
  bool sent_shutdown;
  bool iocblock;
  bool finlock;
  bool rcbset;
  bool wcbset;
  bool again;

  bool verify ();

protected:
  aiossl (int fd, size_t rbufsize);
  ~aiossl ();
  void finalize ();

  bool writing () { return aios::writing () || (weof && !sent_shutdown); }
  int doerr (int err, int slient = false);
  int doinput ();
  int dooutput ();
  void iocb ();
  void setincb ();
  void setoutcb ();

public:
  str cipher;
  str issuer;
  str issuer_dn;
  str subject;
  str subject_dn;

  void startssl (SSL_CTX *ctx, bool server);
  void verify_cb (cbv::ptr cb) { assert (need_handshake); vcb = cb; }
  SSL *get_ssl () { return ssl; }

  static ref<aiossl> alloc (int fd, size_t rbsz = defrbufsize)
  { return New refcounted<aiossl> (fd, rbsz); }
};

#endif /* !_ASYNC_SSL_H_ 1 */
