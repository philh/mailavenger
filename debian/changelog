mailavenger (0.8.5-1) unstable; urgency=medium

  * FTBFS with openssl 1.1.0 (Closes: #828429).
  * Install new systemd service unit.
  * Upgrade to debhelper 11 standard.
  * Bump up Standard-version to 4.2.1.5.
  * Fix some typos from README.Debian.

 -- Ulises Vitulli <dererk@debian.org>  Wed, 05 Dec 2018 08:20:10 -0300

mailavenger (0.8.4-5) unstable; urgency=medium

  * Fix FTBFS with gcc 7. Thanks JuhaniNumminen! (Closes: #871028).
  * Bump up Standard-version to 4.1.5 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Mon, 23 Jul 2018 17:29:34 -0300

mailavenger (0.8.4-4) unstable; urgency=medium

  * The 'waaaat' upload.
  * Added libsasl2-dev build-dep.
  * Fix broken SASL support through hardcode patch.
  * Include Expirable Email Addresses mailavenger support (Closes: #705647).
  * Updated README.Debian documentation to reflect recent changes.
  * Bump up Standard-version to 3.9.6 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Mon, 05 Jan 2015 10:59:58 -0300

mailavenger (0.8.4-3) unstable; urgency=medium

  * Run dh-autoreconf to update config. Thanks Doko@ (Closes: #744628).
  * Bump up Standard-version to 3.9.5 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Fri, 02 May 2014 10:49:45 -0300

mailavenger (0.8.4-2) unstable; urgency=low

  * Improve gcc hardening features:
    - debian/rules: Switch from hardening-includes in favor of dpkg-buildflags.
    - debian/control: Drop build-dep on hardening-includes.
    - debian/patches/handle_errout.patch: Local patch to get rid of
      warn_unused_result Werror.

 -- Ulises Vitulli <dererk@debian.org>  Fri, 16 Aug 2013 09:54:50 +0200

mailavenger (0.8.4-1) unstable; urgency=low

  * New upstream release:
    - Fix FTBFS on gcc 4.8 (Closes: #701317).
    - Added systemd mailavenger.service file.
    - Use new resolver library interface, instead of reloading resolv.conf.
  * Bump up Standard-version to 3.9.4 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Wed, 14 Aug 2013 23:08:43 +0200

mailavenger (0.8.3rc1-1) unstable; urgency=low

  * Fix FTBFS on gcc-4.7 series (Closes: #667268).
  * Fix piuparts QA checks on mailavenger:
    - unowned directory after purge: /var/lib/mailavenger (Closes: #668749).
  * Update Standard-version to 3.9.3 (no changes needed).

 -- Ulises Vitulli <dererk@debian.org>  Sat, 14 Apr 2012 09:40:52 -0300

mailavenger (0.8.2-1) unstable; urgency=low

  * New upstream-coordinated snapshot/release:
    - Fix bdb compatibility on 5.x (Closes: #621440, #634553, #647247)
    - Fix some gcc-4.6 warnings (Closes: #625391).
  * Improve building targets for simplifying portscripts.
  * Clean-up on debian/rules for multiarch support.
  * Dropped local patch for typofixing license documentation.

 -- Ulises Vitulli <dererk@debian.org>  Tue, 13 Dec 2011 09:24:55 -0300

mailavenger (0.8.1-4) unstable; urgency=low

  * Renamed conflicting files to secondary path (Closes: #624234).
  * Update Standard-version to 3.9.2.0 (no changes needed).
  * Clean up old unused targets at debian/rules.
  * Switched to my Debian account.

 -- Ulises Vitulli <dererk@debian.org>  Thu, 16 Jun 2011 18:30:07 -0300

mailavenger (0.8.1-3) unstable; urgency=low

  * The 'Oh-righhht!' upload.
  * debian/rules: Now switching hardening-includes to easily handle gcc's
     advanced protections on unsupported archs (Closes: #580790).

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Thu, 13 May 2010 20:30:39 -0300

mailavenger (0.8.1-2) unstable; urgency=low

  * debian/rules: do not install mac-specific files (Closes: #580397).
  * debian/control: Improved objective package description.
  * debian/copyright: Fix minor typo on Copyright (already fixed on upstream).
  * debian/rules: Disable gcc's Stack Smashing protection for unsupported archs:
    - alpha
    - hppa
    - ia64
    - mips

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Wed, 05 May 2010 16:19:39 -0300

mailavenger (0.8.1-1) unstable; urgency=low

  * Initial release (Closes: #377714).
  * Stick to dpkg-source 3.0 (quilt) format.

 -- Ulises Vitulli <uvitulli@fi.uba.ar>  Thu, 08 Apr 2010 14:11:08 -0300
