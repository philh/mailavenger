=for comment $Id$

=head1 NAME

deliver - deliver mail to a mailbox or maildir spool

=head1 SYNOPSIS

deliver [--umask[=I<val>]] I<destination1> [I<destination2> ...]

=head1 DESCRIPTION

Takes a mail message on standard input, and delivers it to one or more
destination mailboxes.  If a destination ends with a C</> character,
it is interpreted as a qmail maildir format directory (which will be
created if it doesn't already exist).  Otherwise, if the destination
does not end with a C</> character, it is interpreted as a Unix mbox
format file.

If one of the mailboxes is specified as F<->, deliver will send a copy
of the message to its standard output after generating the appropriate
C<From > and C<Return-Path:> lines, if necessary.  This is useful when
piping messages to programs from L<avenger.local(8)|avenger.local(8)>
scripts, as avenger.local does not generate any C<From > or
C<Return-Path: > lines, while deliver will generate these based on the
B<SENDER> environment variable.

=head2 OPTIONS

=over

=item B<--copy>

If deliver cannot seek on its standard input, it will first copy the
message to a temporary file before attempting any deliveries.  Usually
this only occurs when deliver is being fed the output of another
program through a pipe.  The B<--copy> option forces copying
regardless of whether deliver could rewind the file pointer.

=item B<--fcntl> (B<-P>)

This option enables fcntl (a.k.a. POSIX) file locking of mail spools,
in addition to flock and dotfile locking.  The advantage of fcntl
locking is that it may do the right thing over NFS.  However, if
either the NFS client or server does not properly support fcntl
locking, or if the file system is not mounted with the appropriate
options, fcntl locking can fail in one of several ways.  It can allow
different processes to lock the same file concurrently--even on the
same machine.  It can simply hang when trying to acquire a lock, even
if no other process holds a lock on the file.  Also, on some OSes it
can interact badly with flock locking, because those OSes actually
implement flock in terms of fcntl.

=item B<--norewind>

By default, if deliver can rewind its standard input, it will do so
before reading the message.  This lets scripts more easily run several
commands over their standard input when that input is a file.  For
example, a shell script might do the following:

	 if test YES = "`formail -cxz X-Spam-Status:`"; then
	     deliver $HOME/Mail/spam/
	 else
	     deliver $HOME/Mail/ham/
	 fi

The B<--norewind> inhibits that behavior, so that the above script
would likely give unintended results.  B<--norewind> is useful for
testing scripts that aren't supposed to assume they are getting input
from a file.

=item B<--umask>

=item B<--umask=>I<val>

By default, deliver creates all files and directories with a umask value
of 077--meaning files are not readable or writeable by others.  The
B<--umask> option tells deliver to keep whatever umask it was invoked
with.  The B<--umask=>I<val> option tells deliver to use a umask of
I<val>.  Note that to specify I<val> in octal, you must prefix it with
a 0, so the default is equivalent to B<--umask=077>, but not
B<--umask=77>.

=back

=head1 ENVIRONMENT

The following environment variable affects deliver's operation.

=over

=item B<SENDER>

Specifies the envelope sender (bounce address) of the message.  For
maildir format mailboxes, the sender will be included in a
C<Return-Path:> header.  For mbox format mailboxes, the sender is
reflected in the first line, which will contain C<From SENDER ...>.
If B<SENDER> is unspecified, deliver will attempt to extract it from
the first line of the message, if that line begins C<From > or
C<Return-Path:>.  Otherwise, the sender will probably be incorrectly
set.

=back

=head1 EXAMPLES

Using avenger.local, to set up an address as a spam trap that reports
any messages it receives as spam, you might place the following in the
appropriate F<.avenger/local> file:

    | deliver - | spamassassin -r

If you want to reject spam messages during SMTP transactions using
spamassassin, but still want to keep a copy of the spams in
F<$HOME/Mail/spam-log> to keep an eye on how spamassassin is doing,
you might place the line C<bodytest $HOME/.avenger/spam-check>, and
write the F<spam-check> shell script as follows:

    #!/bin/sh
    edinplace -x 111 spamassassin -e 100
    case "$?" in
        0)
            ;;
        100)
            echo Sorry, spamassassin has flagged this message as spam
            deliver $HOME/Mail/spam-log
            exit 100
            ;;
        111)
            echo Sorry, spamassassin has encountered a temporary error
            exit 111
            ;;
        *)
            echo Sorry, spamassassin exited witn an unknown status
            exit 111
            ;;
    esac

Note here that the bodytest script does not need to pipe the message
through C<deliver -> before spamassassin, because bodytest's standard
input does contain C<From > and C<Return-Path:> lines, even though
avenger.local command input does not.

=head1 SEE ALSO

L<avenger(1)|avenger(1)>,
L<dotlock(1)|dotlock(1)>,
L<mailexec(1)|mailexec(1)>,
L<avenger.local(8)|avenger.local(8)>

The Mail Avenger home page: L<http://www.mailavenger.org/>.

=head1 BUGS

When delivering to multiple destinations, if one of them fails,
deliver will halt with a non-zero exit status.  However, it is not
possible to know which destination caused the delivery failure.

To protect against concurrent accesses to mbox format files, deliver
uses both I<flock> and dotfiles to lock mailboxes.  However, it does
not use I<fcntl>/I<lockf>-style locking by default.  Thus, if your
mail reader exclusively uses I<fcntl> for locking, there will be race
conditions unless you specify the B<--fcntl> option.

=head1 AUTHOR

David MaziE<egrave>res
