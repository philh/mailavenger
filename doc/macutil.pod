=for comment $Id$

=head1 NAME

macutil, sendmac - Message Authentication Code utility

=head1 SYNOPSIS

macutil --gen [I<options>]

macutil --sender [I<template>] [--from I<name>] [I<options>]

macutil --check [I<options>] I<code>

macutil [I<options>] --sendmail [I<sendmail-options>]

sendmac [I<sendmail-options>]

=head1 DESCRIPTION

macutil generates and checks the validity of codes that can be
embedded in temporary email addresses.  The codes are calculated using
a secret passphrase stored in a file.  Thus, someone who does not know
the passphrase cannot easily generate a valid code.  Each code has a
configurable expiration time after which it becomes invalid.

To use macutil, you must create a file containing a passphrase.  The
default location of this file is F<$HOME/.avenger/.macpass>, though
the location can be overridden with the B<MACUTIL_PASSFILE>
environment variable or B<--passfile=> command-line option.  The file
should contain a passphrase followed by a newline.  The maximum
allowed length of the passphrase is 64 characters.  B<Do not use your
Unix login password or any password you have used for a sensitive
application, as macutil's password will be stored in cleartext and
thus be relatively easy to compromise.>

Running B<macutil --gen> generates a new I<code> and writes it to
standard output.

Running B<macutil --check> I<code> checks the validity of I<code>.  If
the code is valid and has not expired, macutil exits with status 0.
If the code is invalid or has expired, macutil prints a message to
standard error and exits with a non-zero exit code.

The following options affect macutil's behavior:

=over

=item B<--gen> (B<-g>)

Generates a code, as described above.

=item B<--sender> I<template> (B<-s> I<template>)

This option is like B<--gen>, but outputs a complete email address,
instead of just a code.  The address is formatted based on
I<template>.  I<template> should contain an email address with a C<*>
character.  The C<*> will be replaced by a code.  For example, if
B<template> is C<myname+bounces+*>, running C<macutil --sender> might
output:

    myname+bounces+zjkifk8kuvsy7rubu7vqadmwnn

Don't forget to quote the C<*> character when invoking macutil from a
shell.

=item B<--from> I<name> (B<-f> I<name>)

This option, in conjunction with B<--sender>, produces output more
suitable for the C<From:> field in an email message header.  For
example, if B<name> is set to C<Mail Avenger>, running C<macutil
--sender 'myname+tmp+*host' --from 'Mail Avenger'> might output:

    Mail Avenger <myname+tmp+zjkifk8kuvsy7rubu7vqadmwnn@host>

Note that if the B<MACUTIL_SENDER> environment variable has been set,
this will be used as a default vaule for the B<--sender> option if you
invoke B<macutil --from> and don't specify a B<--sender>.

=item B<--fromexp> I<phrase>

In conjunction with the B<--from> option, this option includes an
expiration time for the address in a comment.  For example, supplying
a I<phrase> of C<address expires> would result in output like this:

    Mail Avenger (address expires 07 Dec 2004)
        <myname+tmp+zjkifk8kuvsy7rubu7vqadmwnn@host>

=item B<--check> (B<-c>)

Checks a code, as described above.  Exits 0 on success; exits non-zero
with a message to standard error if the code is invalid.

=item B<--passfile=>I<file> (B<-p> I<file>)

Specify the passphrase file to use.

Note that if I<file> contains multiple passphrases, one per line,
B<--gen> always uses the first passphrase in the file.  B<--check>,
however, will try all passphrases until one succeeds, and only output
failure if they all fail.  In this way, you can change your
passphrase, but keep accepting the old one for a time by leaving it as
the second line of the file.

=item B<--expire=>I<date>

Specify the expiration date for the code.  I<date> can be an absolute
number of seconds since midnight, Jan 1, 1970, GMT.  Alternatively
(and perhaps more usefully), it can be expressed relative to the
current time, as:

=over

=item B<+>I<num>B<h>

=item B<+>I<num>B<D>

=item B<+>I<num>B<W>

=back

to specify I<num> hours, days, or weeks in the future.  The full range
of suffixes allowed is B<s>, B<m>, B<h>, B<D>, B<W>, B<M>, and B<Y>,
which designate seconds, minutes, hours, days, weeks, months, and
years, respectively.  The default expiration time is 21 days
(C<+21D>).

=item B<--aux=>I<string>

Permutes the algorithm using I<string>.  You must specify the same
B<--aux> argument when both generating and checking codes.  This
allows you to re-use the same password for different sets of codes.
For example, you might require tokens generated with C<macutil --gen
--aux=list1> to be embedded in recipient addresses for one mailing
list, and C<macutil --gen --aux=list2> to be embedded in recipient
addresses for another.  Someone who has an address that is valid for
one list will still not be able to send to the other.

=item B<--date=>I<date>

Run as if the current time were I<date>.  As with B<--expire>, I<date>
can be an absolute number or can be relative to the current time.  Use
B<-> instead of B<+> to specify a time in the past (e.g.,
B<->I<num>I<h> or B<->I<num>I<D>).

=item B<--sendmail>

This option must be the last sendmac option.  It tells macutil to run
sendmail with the remaining arguments you have specified, but to
insert the options B<-f> I<address> at the beginning of the argument
list, where I<address> is generated as with the B<--sender> option.
You must specify an address template, either through explicit use of
the B<--sender> option, or by setting the
B<MACUTIL_SENDER> environment variable.

For example, if B<MACUTIL_SENDER> is C<myname+bounces+*>, running
C<macutil --sendmail friend@domain.com> might run the command:

    sendmail -f \
        myname+bounces+zjkifk8kuvsy7rubu7vqadmwnn \
        friend@domain.com

Note that if invoke the macutil program as C<sendmac> (or as any other
name you link it to beginning with the four letters C<send>), it will
automatically behave as though there were an extra first argument of
B<--sendmail>.  (In this case, you cannot specify any sendmac options,
but you can still control sendmac's behavior through the environment
variables listed below.)

=back

=head1 ENVIRONMENT

=over

=item B<MACUTIL_EXPIRE>

Sets the expiration time if not explicitly overwritten by the
B<--expire> flag.  If B<MACUTIL_EXPIRE> is not set, macutil uses a
default value of C<+21D> (21 days).

=item B<MACUTIL_FROMEXP>

If this option is set to I<phrase>, then the output of C<sendmac
--from> will always behave as though an extra B<--fromexp> I<phrase>
argument had been supplied.

=item B<MACUTIL_PASSFILE>

Specifies a passphrase file other than the default of
F<$HOME/.avenger/.macpass>.

=item B<MACUTIL_SENDER>

Specifies a template sender address to use as a default value of
B<--sender> with the B<--sendmail> and B<--from> options.  See the
descriptions of the B<--sendmail> and B<--from> options above for more
information.

=item B<MACUTIL_SENDMAIL>

Specifies the path to sendmail for the B<--sendmail> option.  The
default is just F<sendmail>.

=back

=head1 FILES

F<$HOME/.avenger/.macpass>

=head1 SEE ALSO

L<avenger(1)|avenger(1)>

The Mail Avenger home page: L<http://www.mailavenger.org/>.

=head1 BUGS

macutil is designed to provide casual security against people trying
to guess a valid temporary email address.  Don't use it where stronger
authentication is required.  In particular, for any given passphrase,
a random code will be valid (at least on some date) with probability 1
in 2^64.  While these are tough odds to beat, cryptographers generally
prefer a margin of safety closer to 1 in 2^128 for high-security
applications (though that would require longer codes).

Someone who sees a valid code can mount an off-line dictionary attack
against your passphrase.  In other words, while it is hard recover
your passphrase outright, given a valid code, it is is easy to verify
whether a particular guess of your passphrase is correct.  By guessing
every word in the dictionary, an attacker can recover weak
passphrases.

Technically, the cryptographic operation performed on the keys is
encryption, not a message authentication code (or MAC).  Hence, one
could argue the utility is misnamed.

=head1 AUTHOR

David MaziE<egrave>res
