=for comment $Id$

=head1 NAME

dotlock - execute a command with a lock on a mailbox

=head1 SYNOPSIS

dotlock [-LPW] I<mbox-file> I<command> [I<arg> ...]

=head1 DESCRIPTION

dotlock acquires a lock on the mailbox file I<mbox-file> using both
I<flock> and a lock file, then executes I<command> with any arguments
specified.  When I<command> exits, dotlock releases the lock.

dotlock attempts to clean up stale lockfiles.  If it succeeds in
locking an I<mbox-file> with I<flock>, and roughly 30 seconds elapse
without there being any changes to I<mbox-file> or the lockfile, then
dotlock will delete the lockfile and try again.

While it holds a lock, lockfile will keep updating the modification
time of the lockfile every 15 seconds, to prevent the lock from
getting cleaned up in the event that I<command> is slow.

=head2 OPTION

=over

=item B<--noflock> (B<-L>)

Ordinarily, dotlock uses both flock and dotfile locking.  (It uses
flock first, but releases that lock in the even that dotfile locking
fails, so as to avoid deadlocking with applications that proceed in
the reverse order.)  The B<-L> option disables flock locking, so that
dotlock only uses dotfile locking.

This is primarily useful as a wrapper around an application that
already does flock locking, but to which you want to add dotfile
locking.  (Even if your mail delivery system doesn't use flock, flock
actually improves the efficiency of dotlock, so there is no reason to
disable it.)

=item B<--fcntl> (B<-P>)

This option enables fcntl (a.k.a. POSIX) file locking of mail spools,
in addition to flock and dotfile locking.  The advantage of fcntl
locking is that it may do the right thing over NFS.  However, if
either the NFS client or server does not properly support fcntl
locking, or if the file system is not mounted with the appropriate
options, fcntl locking can fail in one of several ways.  It can allow
different processes to lock the same file concurrently--even on the
same machine.  It can simply hang when trying to acquire a lock, even
if no other process holds a lock on the file.  Also, on some OSes it
can interact badly with flock locking, because those OSes actually
implement flock in terms of fcntl.

=item B<--nowait> (B<-W>)

With this option, dotlock simply exits non-zero and does not run
I<command> if it cannot immediately acquire the lock.

=back

=head1 SEE ALSO

L<avenger(1)|avenger(1)>,
L<deliver(1)|deliver(1)>,
L<avenger.local(8)|avenger.local(8)>

The Mail Avenger home page: L<http://www.mailavenger.org/>.

=head1 BUGS

dotlock does not perform I<fcntl>/I<lockf>-style locking by default.
Thus, if your mail reader exclusively uses I<fcntl> for locking, there
will be race conditions unless you specify the B<--fcntl> option.

I<flock> does not work over network file systems.  Thus, because of
dotlock's mechanism for cleaning stale lock files, there is a
possibility that a network outage could lead to a race condition where
the lockfile is cleared before I<command> finishes executing.  If
lockfile detects that the lock has been stolen, it prints a message to
standard error, but does not do anything else (like try to kill
I<command>).

=head1 AUTHOR

David MaziE<egrave>res
