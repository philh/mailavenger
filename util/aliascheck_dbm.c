/* $Id$ */

/*
 *
 * Copyright (C) 2004 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "avutil.h"

#ifndef HAVE_NDBM

int
check_alias_dbm (const char *file, const char *user, int map)
{
  return 2;
}

#else /* HAVE_NDBM */

#include <ndbm.h>

int
check_alias_dbm (const char *file, const char *user, int map)
{
  DBM *db;
  datum k, v;
  int len = strlen (file);
  char *dbfile;

  if (len < 4 && strcmp (file + len - 3, ".db"))
    return 2;

  dbfile = xmalloc (len + 1);
  strcpy (dbfile, file);
  dbfile[len - 3] = '\0';
  errno = 0;
  db = dbm_open (dbfile, DBM_RDONLY, 0666);
  free (dbfile);
  if (!db) {
    if (errno)
      perror (file);
    return 2;
  }

  if (!map) {
    /* Only alias files have the special "@" record */
    k.dptr = "@";
    k.dsize = 2;
    v = dbm_fetch (db, k);
    if (!v.dptr) {
      fprintf (stderr, "%s: incomplete/corrupted alias database\n", file);
      dbm_close (db);
      return 2;
    }
  }

  k.dptr = (char *) user;
  k.dsize = strlen (user);
  /* Only alias files include trailing nul byte */
  if (!map)
    k.dsize++;
  v = dbm_fetch (db, k);
  if (!v.dptr) {
    dbm_close (db);
    return 1;
  }
  printf ("%.*s\n", (int) v.dsize, (char *) v.dptr);
  dbm_close (db);
  return 0;
}


#endif /* HAVE_NDBM */
