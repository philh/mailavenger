/* $Id$ */

/*
 *
 * Copyright (C) 2003 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#ifndef _SYNOS_H_
#define _SYNOS_H_ 1

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "vector.h"

#ifdef AVUTIL_MAIN
#include "avutil.h"
#else /* !AVUTIL_MAIN */
#include "sysconf.h"
#endif /* !AVUTIL_MAIN */

extern u_int32_t synos_mtu;

typedef struct tcpopt {
  enum { OPT_NONE, OPT_M, OPT_MMOD, OPT_MSTAR, OPT_N,
	 OPT_T, OPT_T0, OPT_S, OPT_W, OPT_WMOD, OPT_WSTAR } o_type;
  u_int32_t o_val;
} tcpopt_t;

typedef struct fpdat {
  u_int32_t fp_win;
  u_int32_t fp_ttl;
  int fp_df;
  int fp_size;
  VECTOR (tcpopt_t) fp_opts;
} fpdat_t;

typedef struct osdat {
  char *od_name;
  enum { WIN_ANY, WIN_EQ, WIN_MOD, WIN_S, WIN_T } od_wintype;
  u_int32_t od_win;
  u_int32_t od_ttl;
  int od_df;
  int od_size;
  VECTOR (tcpopt_t) od_opts;
} osdat_t;

void synos_clearfp (fpdat_t *fpp);
void synos_clearos (osdat_t *odp);
int synos_check (const fpdat_t *fpp, const osdat_t *odp);
/* Note, you must bzero fdp / odp before calling these functions for
 * the first time, and call synos_clearXX after the last time. */
int synos_parsefp (fpdat_t *fpp, const char *fps);
int synos_parseos (osdat_t *odp, const char *line);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* !_SYNOS_H_ */
