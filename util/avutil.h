/* $Id$ */

/*
 *
 * Copyright (C) 2004 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#ifndef _AVUTIL_H_
#define _AVUTIL_H_ 1

#include <config.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#ifdef HAVE_SYS_FILE_H
# include <sys/file.h>
#endif /* HAVE_SYS_FILE_H */

#ifdef TIME_WITH_SYS_TIME
# include <sys/time.h>
# include <time.h>
#elif defined (HAVE_SYS_TIME_H)
# include <sys/time.h>
#else /* !TIME_WITH_SYS_TIME && !HAVE_SYS_TIME_H */
# include <time.h>
#endif /* !TIME_WITH_SYS_TIME && !HAVE_SYS_TIME_H */

#ifdef HAVE_TIMES
# include <sys/times.h>
#endif /* HAVE_TIMES */

#ifndef HAVE_CLOCK_GETTIME_DECL
int clock_gettime (int, struct timespec *);
#endif /* !HAVE_CLOCK_GETTIME_DECL */

#ifndef HAVE_CLOCK_GETTIME
# undef CLOCK_REALTIME
# undef CLOCK_VIRTUAL
# undef CLOCK_PROF
#endif /* !HAVE_CLOCK_GETTIME */

#ifndef CLOCK_REALTIME
# define CLOCK_REALTIME 0
#endif /* !CLOCK_REALTIME */
#ifndef CLOCK_VIRTUAL
# define CLOCK_VIRTUAL 1
#endif /* !CLOCK_VIRTUAL */
#ifndef CLOCK_PROF
# define CLOCK_PROF 2
#endif /* !CLOCK_PROF */

#ifndef HAVE_INT32_T
typedef int int32_t;
#endif /* !HAVE_INT32_T */
#ifndef HAVE_U_INT32_T
typedef unsigned int u_int32_t;
#endif /* !HAVE_U_INT32_T */

#ifndef HAVE_INT64_T
# if SIZEOF_LONG == 8
typedef long int64_t;
# elif SIZEOF_LONG_LONG == 8
typedef long long int64_t;
# else /* Can't find 64-bit type */
#  error "Cannot find any 64-bit data types"
# endif /* !SIZEOF_LONG_LONG */
#endif /* !HAVE_INT64_T */

#ifndef HAVE_U_INT64_T
# if SIZEOF_LONG == 8
typedef unsigned long u_int64_t;
# elif SIZEOF_LONG_LONG == 8
typedef unsigned long long u_int64_t;
# else /* Can't find 64-bit type */
#  error "Cannot find any 64-bit data types"
# endif /* !SIZEOF_LONG_LONG */
# define HAVE_U_INT64_T 1	/* XXX */
#endif /* !HAVE_INT64_T */

#if STDC_HEADERS
# include <string.h>
# ifndef bzero
#  define bzero(a,b)   memset((a), 0, (b))
# endif /* !bzero */
#else /* !STDC_HEADERS */
# ifndef DMALLOC
#  ifndef HAVE_STRCHR
#   define strchr index
#   define strrchr rindex
#  endif /* !HAVE_STRCHR */
#  ifdef __cplusplus
char *strchr (const char *, int);
char *strrchr (const char *, int);
#  else /* !__cplusplus */
char *strchr ();
char *strrchr ();
#  endif /* !__cplusplus */
#  ifdef HAVE_MEMCPY
#   define bzero(a,b)   memset((a), 0, (b))
#  else /* !HAVE_MEMCPY */
#   define memcpy(d, s, n) bcopy ((s), (d), (n))
#   define memmove(d, s, n) bcopy ((s), (d), (n))
#  endif /* !HAVE_MEMCPY */
# endif /* !DMALLOC */
#endif

#if __GNUC__ < 2 
# ifndef __attribute__
#  define __attribute__(x)
# endif /* !__attribute__ */
#endif /* !gcc 2 */

#include <fcntl.h>
/* constants for flock */
#ifndef LOCK_SH
# define LOCK_SH 1		/* shared lock */
# define LOCK_EX 2		/* exclusive lock */
# define LOCK_NB 4		/* don't block when locking */
# define LOCK_UN 8		/* unlock */
#endif /* !LOCK_SH */
#ifdef NEED_FLOCK_DECL
int flock (int fd, int operation);
#endif /* NEED_FLOCK_DECL */

#ifndef HAVE_PREAD_DECL
ssize_t pread(int, void *, size_t, off_t);
#endif /* !HAVE_PREAD_DECL */
#ifndef HAVE_PWRITE_DECL
ssize_t pwrite(int, const void *, size_t, off_t);
#endif /* !HAVE_PWRITE_DECL */

static inline void
putint (void *_dp, u_int32_t val)
{
  unsigned char *dp = (unsigned char *) (_dp);
  dp[0] = val >> 24;
  dp[1] = val >> 16;
  dp[2] = val >> 8;
  dp[3] = val;
}

static inline u_int32_t
getint (const void *_dp)
{
  const unsigned char *dp = (const unsigned char *) (_dp);
  return dp[0] << 24 | dp[1] << 16 | dp[2] << 8 | dp[3];
}

static inline void
putshort (void *_dp, u_int16_t val)
{
  unsigned char *dp = (unsigned char *) (_dp);
  dp[0] = val >> 8;
  dp[1] = val;
}

static inline u_int16_t
getshort (const void *_dp)
{
  const unsigned char *dp = (const unsigned char *) (_dp);
  return dp[0] << 8 | dp[1];
}

#ifdef DMALLOC
#include <dmalloc.h>
#endif /* DMALLOC */

#ifndef xmalloc
static inline void *
xmalloc (size_t n)
{
  void *p = malloc (n);
  if (!p) {
    fprintf (stderr, "out of memory allocating %lu bytes\n",
	     (unsigned long) n);
    abort ();
  }
  return p;
}
#endif /* xmalloc */

#ifndef xstrdup
static inline char *
xstrdup (const char *s)
{
  char *ret = xmalloc (strlen (s) + 1);
  return strcpy (ret, s);
}
#endif /* !xstrdup */

#ifndef xrealloc
static inline void *
xrealloc (void *p, size_t n)
{
  void *r = realloc (p, n);
  if (!r) {
    fprintf (stderr, "out of memory in xrealloc\n");
    abort ();
  }
  return r;
}
#endif /* !xrealloc */

/* version.c */
void version (const char *prog, int quit);

/* time.c */
long parse_expire (const char *age, time_t now);

/* straux.c */
char *mempbrk (char *, const char *, int);
char *xstrsep (char **, const char *);
char *strnnsep (char **, const char *);

/* aes.c */
struct aes_ctx {
  int nrounds;
  u_int32_t  e_key[60];
  u_int32_t  d_key[60];
};
typedef struct aes_ctx aes_ctx;
void aes_setkey (aes_ctx *aes, const void *key, u_int keylen);
void aes_encrypt (const aes_ctx *aes, void *buf, const void *ibuf);
void aes_decrypt (const aes_ctx *aes, void *buf, const void *ibuf);

/* mdblock.c */
struct mdblock {
  u_int64_t count;
  void (*consume) (struct mdblock *, const unsigned char block[64]);
  unsigned char buffer[64];
};
typedef struct mdblock mdblock;
void mdblock_init (mdblock *mp,
                   void (*consume) (mdblock *, const unsigned char block[64]));
void mdblock_update (mdblock *mp, const void *bytes, size_t len);
void mdblock_finish (mdblock *mp, int bigendian);

/* sha1.c */
struct sha1_ctx {
  mdblock mdb;
  u_int32_t state[5];
};
typedef struct sha1_ctx sha1_ctx;
enum { sha1_hashsize = 20 };
void sha1_init (sha1_ctx *sc);
void sha1_update (sha1_ctx *sc, const void *bytes, size_t len);
void sha1_final (sha1_ctx *sc, unsigned char out[20]);
void hmac_sha1 (void *out, const char *key, const void *data, u_int dlen);

/* armor.c */
char *armor32 (const void *dp, size_t dl);
ssize_t dearmor32len (const char *s);
ssize_t dearmor32 (void *out, const char *s);
char *armor64 (const void *dp, size_t len);
ssize_t dearmor64len (const char *s);
ssize_t dearmor64 (void *out, const char *s);

/* buf.c */
enum lnbuf_res {
  LNBUF_OK = 1,			/* Returned line in buffer */
  LNBUF_EOF = 0,		/* Normal end of file */
  LNBUF_EOFNL = -1,		/* Last line of file has no newline */
  LNBUF_NOMEM = -2,		/* Malloc failed */
  LNBUF_TOOBIG = -3,		/* Maximum requested line size exceeded */
  LNBUF_IOERR = -4,		/* I/O error */
};
struct lnbuf {
  char *buf;			/* Line of text */
  size_t size;			/* Length of line */
  size_t alloc;			/* Number of bytes allocated */
};
int readln (struct lnbuf *res, FILE *fp, size_t maxbuf);

#endif /* !_AVUTIL_H_ */
