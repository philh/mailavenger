/* $Id$ */

/*
 *
 * Copyright (C) 2004-2007 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "avutil.h"

void
version (const char *prog, int quit)
{
  fprintf (stderr, "%s (Mail Avenger) %s\n"
	   "Copyright (C) 2004-2007 David Mazieres\n"
	   "This program comes with NO WARRANTY,"
	   " to the extent permitted by law.\n"
	   "You may redistribute it under the terms of"
	   " the GNU General Public License;\n"
	   "see the file named COPYING for details.\n",
	   prog, VERSION);
  if (quit)
    exit (0);
}
