/* $Id$ */

/*
 *
 * Copyright (C) 2004 David Mazieres (dm@uun.org)
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 *
 */

#include "db.h"

#ifndef DB_CHKSUM
# define DB_CHKSUM DB_CHKSUM_SHA1
#endif /* DB_CHKSUM */

struct dbenv {
  char *home;
  int lockfd;
  DB_ENV *e;
  int txnflag;
};
typedef struct dbenv dbenv;
struct dbexp {
  dbenv *dbe;
  char *path;
  DB *db;
  DB *exp;
};
typedef struct dbexp dbexp;
dbenv *dbenv_alloc (const char *path);
void dbenv_free (dbenv *dbe);
int dbenv_txn (DB_TXN **tid, dbenv *dbe);
int dbenv_commit (DB_TXN *tid);
int dbexp_clean (dbexp *dbx);
int dbexp_free (dbexp *dbx, int sync);
dbexp *dbexp_alloc (dbenv *dbe, const char *path, int rdonly);

